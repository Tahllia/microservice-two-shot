import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Hat from './Hat'
import HatForm from './HatForm'
import Shoes from './Shoes'
import NewShoe from './NewShoe'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<Hat />} />
          <Route path="hats/new" element={<HatForm />} />
          <Route path="shoes" element={<Shoes />} />
          <Route path="shoes/new" element={<NewShoe />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;