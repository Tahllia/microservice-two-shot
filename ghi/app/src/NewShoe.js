import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            manufacturer: "",
            color: "",
            pictureUrl: "",
            bins: [],
            };
            this.handleNameChange = this.handleNameChange.bind(this);
            this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
            this.handleColorChange = this.handleColorChange.bind(this);
            this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
            this.handleBinChange = this.handleBinChange.bind(this);
            this.handleSubmit = this.handleSubmit.bind(this);
                        }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
        }

        handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value})
        }

        handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
        }

        handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({pictureUrl: value})
        }

        handleBinChange(event) {
        const value = event.target.value;
        this.setState({bin: value})
        }

    async handleSubmit(event) {
        event.preventDefault();
        console.log({...this.state})
        const data = {...this.state};
        data.picture_url = data.pictureUrl;
        delete data.pictureUrl;
        delete data.bins;
        console.log(data);
        
        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            const cleared = {
                name: "",
                manufacturer: "",
                color: "",
                pictureUrl: "",
                bin: ""
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
    
        const response = await fetch(url);
        // console.log(response)
    
        if (response.ok) {
            const data = await response.json();
            console.log("Data", data)
            this.setState({bins: data.bins});
        }}
        



        
render() {
    return (
    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
        <h1>Create a new shoe</h1>
        <form onSubmit={this.handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
            <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
            <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
            <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
            <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
            <input onChange={this.handleColorChange} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
            <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
            </div>
            <div className="form-floating mb-3">
            <input onChange={this.handlePictureUrlChange} value={this.state.pictureUrl} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
            <label htmlFor="picture_url">Picture: URL</label>
            </div>
            <div className="mb-3">
            <select onChange={this.handleBinChange} value={this.state.bin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin location</option>
                {this.state.bins.map(bin => {
                        return (
                        <option key={bin.id} value={bin.href}>
                            {bin.closet_name}
                        </option>
                        );
                    })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
        </form>
        </div>
    </div>
    </div>
    );
}
}

export default ShoeForm;