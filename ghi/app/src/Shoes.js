import React from 'react';
import { Link } from 'react-router-dom';


function ShoeColumn(props) {
return (
<div className="col">
    {props.list.map(data => {
        //console.log("funct ShoeColumn Data***", data)
    const shoe = data;
    //console.log("Shoe Data?", shoe)
    //console.log("funct ShoeColumn Data***", data)
    return (
        <div key={shoe.href} className="card mb-3 shadow">
            <img src={shoe.picture_url} className="card-img-top" />
            <div className="card-body">
                <h5 className="card-title">{shoe.name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                {shoe.bin.closet_name}
                </h6>
                <p className="card-text">
                {shoe.manufacturer}
                </p>
                <p>{shoe.color}</p>
        </div>
        <div className="card-footer">
        <button onClick={() => ShoeDelete(shoe.pk)} type="button" className="btn btn-danger">
                    delete
                </button>
        </div>
        </div>
    );
    })}
</div>
);
}


async function ShoeDelete(pk){
    const shoeURL=`http://localhost:8080/api/shoes/${pk}`;
    const fetchConfig = {
        method: "delete"
    }
    const response = await fetch(shoeURL, fetchConfig);
    if(response.ok){
        await response.json();
        console.log("Delete: Sucessful")
    }

}




class MainPage extends React.Component {
constructor(props) {
super(props);
this.state = {
    shoeColumns: [],
};
}

async componentDidMount() {
const url = 'http://localhost:8080/api/shoes/ ';

try {
    const response = await fetch(url);
    if (response.ok) {
    // Get the list of shoes
    const data = await response.json();

    // Create a list of for all the requests and
    // add all of the requests to it
    const requests = [];
    //console.log("Data*****", data)
    for (let shoe of data.shoes) {
        //console.log("********Shoe*******------", shoe)
        const detailUrl = `http://localhost:8080/api/shoes/${shoe.pk}`;
        requests.push(fetch(detailUrl));
    }

    // Wait for all of the requests to finish
    // simultaneously
    const responses = await Promise.all(requests);

    // Set up the "columns" to put the shoe
    // information into
    const shoeColumns = [[],[],[]];

    // Loop over the shoe detail responses and add
    // each to to the proper "column" if the response is
    // ok
    let i = 0;
    for (const shoeResponse of responses) {
        if (shoeResponse.ok) {
        const details = await shoeResponse.json();
        //console.log("Details", details)
        shoeColumns[i].push(details);
        //console.log("shoeColumns:", shoeColumns)
        i = i + 1;
        if (i > 2) {
            i = 0;
        }
        } else {
        console.error(shoeResponse);
        }
    }

    // Set the state to the new list of three lists of
    // shoes
    this.setState({shoeColumns: shoeColumns});
    }
} catch (e) {
    console.error(e);
}
}

// Need to change like all of this
render() {
return (
    <>
    <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Shoes!</h1>
        <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
            Need to keep track of your shoes and hats? We have
            the solution for you!
        </p>
        </div>
    </div>
    <div className="container">
        <div className="row">
        {this.state.shoeColumns.map((shoeList, index) => {
            return (
            <ShoeColumn key={index} list={shoeList} />
            );
        })}
        </div>
    </div>
    </>
);
}
}

export default MainPage;