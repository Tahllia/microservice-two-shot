import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            styleName: '',
            fabric: '',
            color: '',
            pictureUrl: '',
            locations: []
          };
          this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
          this.handleFabricChange = this.handleFabricChange.bind(this);
          this.handleColorChange = this.handleColorChange.bind(this);
          this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
          this.handleLocationChange = this.handleLocationChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({styleName: value})
        }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value})
        }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
        }

    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({pictureUrl: value})
        }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
        }

    async handleSubmit(event) {
        event.preventDefault();
        console.log({...this.state})
        const data = {...this.state};
        data.style_name = data.styleName;
        delete data.styleName;
        data.picture_url = data.pictureUrl;
        delete data.pictureUrl;
        delete data.locations;
        
        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            const cleared = {
                styleName: '',
                fabric: '',
                color: '',
                pictureUrl: '',
                location: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
    
        const response = await fetch(url);
        // console.log(response)
    
        if (response.ok) {
          const data = await response.json();
          console.log(data)
          this.setState({locations: data.locations});
          
        }
      }

    render() {
      return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new hat</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleStyleNameChange} value={this.state.styleName} placeholder="styleName" required type="text" name="styleName" id="styleName" className="form-control"/>
                <label htmlFor="name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="starts">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="ends">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureUrlChange} value={this.state.pictureUrl} placeholder="pictureUrl" required type="url" name="pictureUrl" id="pictureUrl" className="form-control"/>
                <label htmlFor="max_presentations">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} value={this.state.location} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                            return (
                            <option key={location.id} value={location.href}>
                                {location.closet_name}
                            </option>
                            );
                        })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
    }
  }

export default HatForm;
