import React from 'react';
import { Link } from 'react-router-dom';


function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map(data => {
        const hat = data;
        return (
          <div key={hat.href} className="card mb-3 shadow">
            <img src={hat.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{hat.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                Closet: {hat.location.closet_name}
              </h6>
              <p className="card-text">
                Fabric: {hat.fabric}<br />
                Color: {hat.color}
              </p>
            </div>
            <div className="card-footer">
                <button onClick={() => handleHatDelete(hat.pk)} type="button" className="btn btn-danger">
                    delete
                </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

async function handleHatDelete(pk) {
    const hatUrl = `http://localhost:8090/api/hats/${pk}`;
        const fetchConfig = {
            method: "delete",
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            console.log("IT DELETED!?!?")
        }
}

class Hat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      HatColumns: [],
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats';

    try {
      const response = await fetch(url);
      if (response.ok) {
        // Get the list of hats
        const data = await response.json();
        // Create a list of for all the requests and
        // add all of the requests to it
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090/api/hats/${hat.pk}`;
          requests.push(fetch(detailUrl));
        }

        // Wait for all of the requests to finish
        // simultaneously
        const responses = await Promise.all(requests);

        // Set up the "columns" to put the hat
        // information into
        const HatColumns = [[], [], []];

        // Loop over the hat detail responses and add
        // each to to the proper "column" if the response is
        // ok
        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            HatColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }

        // Set the state to the new list of three lists of
        // hats
        this.setState({HatColumns: HatColumns});
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Hats</h1>
        <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
            </p>
        </div>
        </div>
        <div className="container">
          <h2></h2>
          <div className="row">
            {this.state.HatColumns.map((hatList, index) => {
              return (
                <HatColumn key={index} list={hatList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default Hat;
