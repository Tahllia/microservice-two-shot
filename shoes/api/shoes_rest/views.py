from urllib import response
from django.http import JsonResponse

from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinsVO, Shoes

# Create your views here.
class BinVODetailEncoder(ModelEncoder):
    model = BinsVO
    properties = ["closet_name", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = ["name", "pk"]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
        "pk"]
    
    encoders = {
        "bin": BinVODetailEncoder(),
    }

    # def get_extra_data(self, o):        ????????????
    #     return super().get_extra_data(o)

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method =="GET":
        try:
            shoes = Shoes.objects.all()
            return JsonResponse(
            {"shoes": shoes}, encoder = ShoeListEncoder)
        except: 
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
    else:
        content = json.loads(request.body)
        print("Content**********", content)
        try:
            bin_href = content["bin"]
            bin = BinsVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin href"},
                status=400,
            )

        shoe = Shoes.objects.create(**content)    
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

@require_http_methods(["DELETE", "GET", "PUT"])
def api_shoes_detail(request, pk):
    if request.method == "GET":
        try: 
            shoe = Shoes.objects.get(id=pk)
            return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder, safe=False,)
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoes.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

